﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Transport;
using sniffer_application.Network.IPv4;
using sniffer_application.DataLink;
using sniffer_application.Database;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Collections.Concurrent;
using sniffer_application.Utils;

namespace sniffer_application.Analyzer
{
    public class HttpGuardian : ThreadedQueue<SegmentTCP>
    {
        public HttpGuardian()
        {

        }
       
        protected override void Use(SegmentTCP segment)
        {
            if (segment.DestinationPort != 80 && segment.DestinationPort != 443) return;
            if (!segment.UpperLevel.UpperLevel.DestinationMAC.Equals(Global.RouterMAC))
            {
                //Ignore entering http request (only outbound)
                return;
            }
            if (segment.DestinationPort == 80)
            {
                string data = Encoding.UTF8.GetString(segment.Payload());
                if (data.Contains("GET"))
                {
                    string[] c = data.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    AddressIPv4 ipv4 = segment.UpperLevel.DestinationAddress as AddressIPv4;
                    //AddressIPv6 ipv6 = segment.UpperLevel.DestinationAddress as AddressIPv6;
                    AddressMAC sourceMac = segment.UpperLevel.UpperLevel.SourceMAC;

                    string get = "";
                    string host = "";
                    for (int i = 0; i < c.Length; i++)
                    {
                        if (c[i].Contains("GET"))
                        {
                            get = c[i].Replace("GET", "").Trim();
                        }
                        if (c[i].ToLower().Contains("host:"))
                        {
                            host = c[i].ToLower().Replace("host:", "").Trim();
                        }
                    }

                    try
                    {
                        SqlConnection conn = DBUtils.CreateConnection();
                        int hostId = DBUtils.GetOrCreateHost(sourceMac, conn);
                        int ipv4Id = DBUtils.GetOrCreateIpv4(ipv4, conn);

                        Console.WriteLine("HTTP: " + host + get);
                        SqlCommand myCommand = new SqlCommand("INSERT INTO HTTP (Date, URL, IsSecure, IdNetworkAddress, IdHost) Values (@P1, @p2, @P3, @P4, @P5)", conn);
                        myCommand.Parameters.Add(new SqlParameter("@P1", SqlDbType.DateTime) { Value = DateTime.Now });
                        myCommand.Parameters.Add(new SqlParameter("@P2", SqlDbType.VarChar) { Value = host + get });
                        myCommand.Parameters.Add(new SqlParameter("@P3", SqlDbType.Bit) { Value = false });
                        myCommand.Parameters.Add(new SqlParameter("@P4", SqlDbType.Int) { Value = ipv4Id });
                        myCommand.Parameters.Add(new SqlParameter("@P5", SqlDbType.Int) { Value = hostId });

                        myCommand.ExecuteNonQuery();
                        myCommand.Dispose();
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("HTTP GUARDIAN: " + ex.Message);
                    }
                }
            }
        }

        protected override void HandleException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("HttpGuardian: " + ex.Message);
            Console.ResetColor();
        }
    }
}
