﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Transport;
using sniffer_application.Utils;
using sniffer_application.Stack_TCP_IP.Application.DNS;
using System.Data.SqlClient;
using sniffer_application.Database;
using System.Data;
using sniffer_application.Network.IPv4;

namespace sniffer_application.Analyzer
{
    public class DnsGuardian : ThreadedQueue<DatagramUDP>
    {
        private List<DnsMessage> requests;
        private List<DateTime> times;
        private List<int> ids;
        public DnsGuardian()
        {
            this.requests = new List<DnsMessage>();
            this.times = new List<DateTime>();
            this.ids = new List<int>();
        }
        protected override void Use(DatagramUDP elem)
        {
            DnsMessage dns = DnsMessage.ToDnsMessage(elem);
            if (dns == null) return;

            bool exit = false;
            for (int i = 0; i < requests.Count; i++)
            {
                if (requests[i].TransactionID == dns.TransactionID &&
                    requests[i].UpperLevel.SourcePort == dns.UpperLevel.DestinationPort &&
                    requests[i].UpperLevel.DestinationPort == dns.UpperLevel.SourcePort)
                {

                    Console.WriteLine("DNS: query: " + requests[i] + " solved with: " + dns);
                    AddAnswers(dns.Answers, ids[i]);

                    requests.RemoveAt(i);
                    times.RemoveAt(i);
                    ids.RemoveAt(i);
                    i--;

                    exit = true;
                    continue;
                }

                if ((DateTime.Now - times[i]).TotalSeconds > 60)
                {
                    Console.WriteLine("DNS: " + requests[i] + " expired");
                    requests.RemoveAt(i);
                    times.RemoveAt(i);
                    ids.RemoveAt(i);
                    i--;
                    continue;
                }
            }

            if (exit)
            {
                Console.WriteLine("DNS: Total requests dns pending: " + requests.Count);
                return;
            }


            if (dns.UpperLevel.DestinationPort == 53)
            {
                Console.WriteLine("DNS: new query: " + dns);
                requests.Add(dns);
                times.Add(DateTime.Now);
                ids.Add(AddQueryAndQuestions(dns));
            }
        }

        private int AddQueryAndQuestions(DnsMessage message)
        {
            SqlConnection conn = DBUtils.CreateConnection();
            int addrIpv4Id = DBUtils.GetOrCreateIp(message.UpperLevel.UpperLevel.DestinationAddress, conn);
            int addrMacId = DBUtils.GetOrCreateHost(message.UpperLevel.UpperLevel.UpperLevel.SourceMAC, conn);

            string query = "INSERT INTO DnsQuery (Date, TransactionID, IdHost, IdServerNetworkAddress) OUTPUT INSERTED.ID VALUES(@DATE, @TRID, @IDH, @IDN)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.Add(new SqlParameter("@DATE", SqlDbType.DateTime) { Value = DateTime.Now });
            cmd.Parameters.Add(new SqlParameter("@TRID", SqlDbType.Int) { Value = message.TransactionID });
            cmd.Parameters.Add(new SqlParameter("@IDH", SqlDbType.Int) { Value = addrMacId });
            cmd.Parameters.Add(new SqlParameter("@IDN", SqlDbType.Int) { Value = addrIpv4Id });
            int idQuery = (int)cmd.ExecuteScalar();
            cmd.Dispose();

            query = "INSERT INTO DnsQuestionSection (QName, QType, QClass, IdQuery) VALUES(@NAME, @TYPE, @CLASS, @ID)";
            QuestionSection[] quests = message.Questions;
            for (int i = 0; i < quests.Length; i++)
            {
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar) { Value = quests[i].Name });
                cmd.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.Int) { Value = (int)quests[i].Type });
                cmd.Parameters.Add(new SqlParameter("@CLASS", SqlDbType.Int) { Value = (int)quests[i].Class });
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int) { Value = idQuery });
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }

            conn.Close();
            return idQuery;
        }
        private void AddAnswers(ResourceRecord[] answers, int idQuery)
        {
            SqlConnection conn = DBUtils.CreateConnection();


            for (int i = 0; i < answers.Length; i++)
            {
                string query;
                SqlCommand cmd;
                if (answers[i].Type == RRType.A)
                {
                    query = "INSERT INTO DnsResourceRecordA (Name, Class, TTL, Value, IdQuery) VALUES(@NAME, @CLASS, @TTL, @VAL, @ID)";
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.Add(new SqlParameter("@VAL", SqlDbType.Int) { Value = DBUtils.GetOrCreateIpv4((AddressIPv4)answers[i].ResourceDataObj, conn) });
                    
                }
                else if (answers[i].Type == RRType.CNAME)
                {
                    query = "INSERT INTO DnsResourceRecordCNAME (Name, Class, TTL, Value, IdQuery) VALUES(@NAME, @CLASS, @TTL, @VAL, @ID)";
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.Add(new SqlParameter("@VAL", SqlDbType.VarChar) { Value = answers[i].ResourceDataObj });
                }
                else
                {
                    //NOT SUPPORTED YET
                    continue;
                }
              
                

                cmd.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar) { Value = answers[i].Name });
                cmd.Parameters.Add(new SqlParameter("@CLASS", SqlDbType.Int) { Value = (int)answers[i].Class });
                cmd.Parameters.Add(new SqlParameter("@TTL", SqlDbType.BigInt) { Value = (long)answers[i].TTL });
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int) { Value = idQuery });
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }

            conn.Close();
        }

        protected override void HandleException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("DnsGuardian: " + ex.Message);
            Console.ResetColor();
        }
    }
}
