﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Network;
using sniffer_application.Network.IPv4;
using sniffer_application.DataLink;
using sniffer_application.Transport;
using sniffer_application.Database;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using sniffer_application.Utils;

namespace sniffer_application.Analyzer
{
    public class ContactUpdater : ThreadedQueue<TransportSegment>
    {
        private Timer timer;
        private Dictionary<ContactKey, ContactValue> contact = new Dictionary<ContactKey, ContactValue>();
        private readonly object syncLock = new object();

        public ContactUpdater()
        {
            TimeSpan tickSpan = TimeSpan.FromSeconds(1);
            timer = new Timer(Update, null, tickSpan, tickSpan);
        }
        private void Update(Object obj)
        {
            try
            {
                Dictionary<ContactKey, ContactValue> contact;
                lock (syncLock)
                {
                    contact = new Dictionary<ContactKey, ContactValue>(this.contact);
                    this.contact.Clear();
                }

                string query =
    @"begin tran
if exists (SELECT * FROM Contact WHERE PortNumber = @port AND IdHost = @host AND IdNetworkAddress = @ip)
begin
   UPDATE Contact SET LastVisit = @date, ContactsCount = ContactsCount + @count
   WHERE PortNumber = @port AND IdHost = @host AND IdNetworkAddress = @ip
end
else
begin
   INSERT INTO Contact (LastVisit, PortNumber, IdHost, IdNetworkAddress, ContactsCount)
   VALUES (@date, @port, @host, @ip, @count)
end
commit tran";

                var conn = DBUtils.CreateConnection();
                foreach (var p in contact)
                {
                    SqlCommand myCommand = new SqlCommand(query, conn);
                    myCommand.Parameters.Add(new SqlParameter("@date", SqlDbType.DateTime) { Value = p.Value.LastContact });
                    myCommand.Parameters.Add(new SqlParameter("@port", SqlDbType.Int) { Value = p.Key.Port });
                    myCommand.Parameters.Add(new SqlParameter("@host", SqlDbType.Int) { Value = p.Key.IdMAC });
                    myCommand.Parameters.Add(new SqlParameter("@ip", SqlDbType.Int) { Value = p.Key.IdIP });
                    myCommand.Parameters.Add(new SqlParameter("@count", SqlDbType.BigInt) { Value = p.Value.ContactQuantity });

                    myCommand.ExecuteNonQuery();
                    myCommand.Dispose();
                }

                conn.Close();
                Console.WriteLine("Flushed " + contact.Sum(p => p.Value.ContactQuantity) + " contacts in " + contact.Count + " key");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        private class ContactKey
        {
            public int IdMAC, IdIP, Port;
            public ContactKey(int idMAC, int idIP, int port)
            {
                this.IdMAC = idMAC;
                this.IdIP = idIP;
                this.Port = port;
            }

            public override int GetHashCode()
            {
                return IdMAC * IdIP * Port + IdMAC + IdIP + Port;
            }
            public override bool Equals(object obj)
            {
                ContactKey t = obj as ContactKey;
                if (t == null) return false;

                return t.IdMAC == IdMAC && t.IdIP == IdIP && t.Port == Port;
            }
        }
        private class ContactValue
        {
            public long ContactQuantity;
            public DateTime LastContact;

            public ContactValue()
            {
                ContactQuantity = 1;
                LastContact = DateTime.Now;
            }

            public void Contact()
            {
                LastContact = DateTime.Now;
                ContactQuantity++;
            }
        }

        protected override void Use(TransportSegment segment)
        {
            NetworkPacket packet = segment.UpperLevel;
            if (packet is PacketIPv4)
            {
                PacketIPv4 ipv4 = packet as PacketIPv4;

                NetworkAddress contactedIP;
                AddressMAC host;
                int port;

                if (ipv4.SourceAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask) && !ipv4.DestinationAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask))
                {
                    //source address è il local address, quindi contatto in uscita
                    contactedIP = ipv4.DestinationAddressIPv4;
                    port = segment.DestinationPort;
                    host = packet.UpperLevel.SourceMAC;
                }
                else if (ipv4.DestinationAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask) && !ipv4.SourceAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask))
                {
                    //destination address è il local address, quindi contatto in ingresso
                    contactedIP = ipv4.SourceAddressIPv4;
                    port = segment.SourcePort;
                    host = packet.UpperLevel.DestinationMAC;
                }
                else
                {
                    //nothing
                    return;
                }

                var conn = DBUtils.CreateConnection();
                int idIP = DBUtils.GetOrCreateIp(contactedIP, conn);
                int idMAC = DBUtils.GetOrCreateHost(host, conn);
                conn.Close();
                ContactKey key = new ContactKey(idMAC, idIP, port);
                lock (syncLock)
                {
                    if (contact.ContainsKey(key))
                    {
                        contact[key].Contact();
                    }
                    else
                    {
                        contact.Add(key, new ContactValue());
                    }
                }
            }
            else
            {
                throw new Exception("Contact updater supports only Ipv4 for now");
            }
        }
        protected override void HandleException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ContactUpdater: " + ex.Message);
            Console.ResetColor();
        }
    }
}
