﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Network;
using sniffer_application.Network.IPv4;
using sniffer_application.DataLink;
using sniffer_application.Database;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Collections.Concurrent;
using sniffer_application.Utils;

namespace sniffer_application.Analyzer
{
    public class BindingUpdater : ThreadedQueue<NetworkPacket>
    {
        private Dictionary<NetworkAddress, AddressMAC> currentBinds = new Dictionary<NetworkAddress, AddressMAC>();

        public BindingUpdater()
        {
            SqlConnection conn = DBUtils.CreateConnection();
            string query = "SELECT MacAddress,Ipv4,Ipv6 FROM Binding,NetworkAddress,Host WHERE Binding.StopDate is NULL AND Binding.IdHost = Host.Id AND Binding.IdNetworkAddress = NetworkAddress.Id;";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            Console.WriteLine("BindingUpdater init...");
            while (reader.Read())
            {
                AddressMAC addrMac = new AddressMAC(reader.GetSqlBytes(0).Value);
                if (!reader.IsDBNull(1))
                {
                    AddressIPv4 addrIpv4 = new AddressIPv4(reader.GetSqlBytes(1).Value);
                    currentBinds.Add(addrIpv4, addrMac);
                    Console.WriteLine(addrIpv4 + " -> " + addrMac);
                }
                else if (!reader.IsDBNull(2))
                {
                    throw new Exception("Ipv6 is not supported yet (BindingUpdater)");
                }
                
            }
            reader.Close();
            cmd.Dispose();
            conn.Close();
        }

        protected override void Use(NetworkPacket packet)
        {
            if (packet is PacketIPv4)
            {
                PacketIPv4 ipv4 = packet as PacketIPv4;

                AddressIPv4 addrIpv4;
                AddressMAC addrMac;

                if (ipv4.SourceAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask) && !ipv4.DestinationAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask))
                {
                    //source address è il local address
                    addrIpv4 = ipv4.SourceAddressIPv4;
                    addrMac = ipv4.UpperLevel.SourceMAC;
                }
                else if (ipv4.DestinationAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask) && !ipv4.SourceAddressIPv4.SameSubnetAs(Global.IPv4Subnet, Global.IPv4SubnetMask))
                {
                    //destination address è il local address, quindi contatto in ingresso
                    addrIpv4 = ipv4.DestinationAddressIPv4;
                    addrMac = ipv4.UpperLevel.DestinationMAC;
                }
                else
                {
                    //nothing
                    return;
                }

                UpdateStatus(addrIpv4, addrMac);
            }
            else
            {
                throw new Exception("Binding updater supports only Ipv4 for now");
            }
        }
        private void UpdateStatus(NetworkAddress networkAddress, AddressMAC macAddress)
        {
            /*
             * Un indirizzo di rete può:
             *  - Essere libero                 -> aprire un nuovo bind
             *  - Essere occupato da un altro   -> chiudere vecchio bind aprirne uno nuovo
             *  - Essere occupato da me         -> nulla
             * */

            if (currentBinds.ContainsKey(networkAddress))
            {
                if (currentBinds[networkAddress].Equals(macAddress))
                {
                    //già occupato da me, niente da fare
                    return;
                }
                else
                {
                    //occupato da un altro, chiudere e creare uno nuovo
                    Console.WriteLine("BINDING: " + networkAddress + " from " + currentBinds[networkAddress] + " to " + macAddress);
                    StopBinding(networkAddress);
                    currentBinds.Remove(networkAddress);
                    CreateNewBinding(networkAddress, macAddress);
                    return;
                }
            }
            else
            {
                //creare uno nuovo
                Console.WriteLine("BINDING: (new)" + networkAddress + " -> " + macAddress);
                CreateNewBinding(networkAddress, macAddress);
                return;
            }
        }
        private void StopBinding(NetworkAddress networkAddress)
        {
            SqlConnection conn = DBUtils.CreateConnection();
            int addrIpv4Id = DBUtils.GetOrCreateIp(networkAddress, conn);
            string query = "UPDATE Binding SET StopDate = @SD WHERE StopDate is NULL AND IdNetworkAddress = @IDN";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.Add(new SqlParameter("@SD", SqlDbType.DateTime) { Value = DateTime.Now });
            cmd.Parameters.Add(new SqlParameter("@IDN", SqlDbType.Int) { Value = addrIpv4Id });
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            conn.Close();
        }
        private void CreateNewBinding(NetworkAddress networkAddress, AddressMAC macAddress)
        {
            currentBinds.Add(networkAddress, macAddress);
            SqlConnection conn = DBUtils.CreateConnection();
            int addrIpv4Id = DBUtils.GetOrCreateIp(networkAddress, conn);
            int addrMacId = DBUtils.GetOrCreateHost(macAddress, conn);
            string query = "INSERT INTO Binding (StartDate, IdHost, IdNetworkAddress) VALUES(@SD, @IDH, @IDN)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.Add(new SqlParameter("@SD", SqlDbType.DateTime) { Value = DateTime.Now });
            cmd.Parameters.Add(new SqlParameter("@IDH", SqlDbType.Int) { Value = addrMacId });
            cmd.Parameters.Add(new SqlParameter("@IDN", SqlDbType.Int) { Value = addrIpv4Id });
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            conn.Close();
        }

        protected override void HandleException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("BindingUpdater: " + ex.Message);
            Console.ResetColor();
        }
    }
    

   
}
