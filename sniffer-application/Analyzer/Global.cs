﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.DataLink;
using sniffer_application.Network.IPv4;

namespace sniffer_application.Analyzer
{
    public static class Global
    {
        public static AddressMAC RouterMAC = new AddressMAC(new byte[] { 0x10 , 0x13 , 0x31, 0x55 , 0x63 , 0xd4 }, 0);
        public static AddressIPv4 IPv4SubnetMask = new AddressIPv4(255, 255, 255, 0);
        public static AddressIPv4 IPv4Subnet = new AddressIPv4(192, 168, 1, 0);
    }
}
