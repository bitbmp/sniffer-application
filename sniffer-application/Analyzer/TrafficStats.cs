﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.DataLink;
using System.Data.SqlClient;
using sniffer_application.Database;
using System.Collections.Concurrent;
using System.Threading;
using System.Data;
using sniffer_application.Utils;

namespace sniffer_application.Analyzer
{
    class Stat
    {
        public long DownloadedBytes;
        public long UploadedBytes;
        public long DownloadedPackets;
        public long UploadedPackets;

        public bool TouchedSec = false;
        public bool TouchedMin = false;
        public bool TouchedHour = false;
        public bool TouchedDay = false;

        public void Touch()
        {
            TouchedSec = TouchedMin = TouchedHour = TouchedDay = true;
        }
    }
    public class TrafficStats : ThreadedQueue<FrameEthernet>
    {
        private Dictionary<AddressMAC, Stat> stats = new Dictionary<AddressMAC, Stat>();
        private readonly object syncLock = new object();

        private Timer timer;
        private AddressMAC totalMac = new AddressMAC(new byte[6], 0);
        public TrafficStats()
        {
            SqlConnection conn = DBUtils.CreateConnection();
            SqlCommand cmd = new SqlCommand(DBUtils.GetLastSecStat_QUERY, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            Stat total = new Stat();
            while (reader.Read())
            {
                stats.Add(new AddressMAC(reader.GetSqlBinary(0).Value), new Stat()
                    {
                        DownloadedBytes = reader.GetInt64(1),
                        UploadedBytes = reader.GetInt64(2),
                        DownloadedPackets = reader.GetInt64(3),
                        UploadedPackets = reader.GetInt64(4),
                    });

                if (!new AddressMAC(reader.GetSqlBinary(0).Value).Equals(totalMac))
                {
                    total.DownloadedBytes += reader.GetInt64(1);
                    total.UploadedBytes += reader.GetInt64(2);
                    total.DownloadedPackets += reader.GetInt64(3);
                    total.UploadedPackets += reader.GetInt64(4);
                }
            }

            if (!stats.ContainsKey(totalMac))
            {
                stats.Add(totalMac, total);
            }
            reader.Close();
            reader.Dispose();
            cmd.Dispose();


            foreach (AddressMAC m in DBUtils.GetHostsTouched(conn, "MinStat"))
                stats[m].TouchedMin = true;
            foreach (AddressMAC m in DBUtils.GetHostsTouched(conn, "HourStat"))
                stats[m].TouchedHour = true;
            foreach (AddressMAC m in DBUtils.GetHostsTouched(conn, "DayStat"))
                stats[m].TouchedDay = true;

            conn.Close();

            TimeSpan tickSpan = TimeSpan.FromSeconds(0.01);
            timer = new Timer(Update, null, tickSpan, tickSpan);
        }
        private void Update(Object obj)
        {
            try
            {
                lock (syncLock)
                {
                    DateTime now = DateTime.Now;
                    if (currentSec != now.Second)
                    {
                        FlushStats("SecStat", now);
                        currentSec = now.Second;
                    }
                    if (currentMin != now.Minute)
                    {
                        FlushStats("MinStat", now);
                        currentMin = now.Minute;
                    }
                    if (currentHour != now.Hour)
                    {
                        FlushStats("HourStat", now);
                        currentHour = now.Hour;
                    }
                    if (currentDay != now.Day)
                    {
                        FlushStats("DayStat", now);
                        currentDay = now.Day;
                    }
                }
            }
            catch (Exception ex) 
            {
                HandleException(ex);
            }
        }

        private void FlushStats(string table, DateTime now)
        {
            Stat total = new Stat();
            SqlConnection conn = DBUtils.CreateConnection();
            if (conn == null) return;

            int hits = 0;
            now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            foreach (var p in stats)
            {
                if (p.Value.TouchedSec && table == "SecStat") p.Value.TouchedSec = false;
                else if (table == "SecStat") continue;

                if (p.Value.TouchedMin && table == "MinStat") p.Value.TouchedMin = false;
                else if (table == "MinStat") continue;

                if (p.Value.TouchedHour && table == "HourStat") p.Value.TouchedHour = false;
                else if (table == "HourStat") continue;

                if (p.Value.TouchedDay && table == "DayStat") p.Value.TouchedDay = false;
                else if (table == "DayStat") continue;


                int hostId = DBUtils.GetOrCreateHost(p.Key, conn);

                SqlCommand myCommand = new SqlCommand("INSERT INTO " + table + " (Date, DownloadedBytes, UploadedBytes, DownloadedPackets, UploadedPackets, IdHost) Values (@P1, @p2, @P3, @P4, @P5, @P6)", conn);
                myCommand.Parameters.Add(new SqlParameter("@P1", SqlDbType.DateTime) { Value = now });
                myCommand.Parameters.Add(new SqlParameter("@P2", SqlDbType.BigInt) { Value = p.Value.DownloadedBytes });
                myCommand.Parameters.Add(new SqlParameter("@P3", SqlDbType.BigInt) { Value = p.Value.UploadedBytes });
                myCommand.Parameters.Add(new SqlParameter("@P4", SqlDbType.BigInt) { Value = p.Value.DownloadedPackets });
                myCommand.Parameters.Add(new SqlParameter("@P5", SqlDbType.BigInt) { Value = p.Value.UploadedPackets });
                myCommand.Parameters.Add(new SqlParameter("@P6", SqlDbType.Int) { Value = hostId });

                myCommand.ExecuteNonQuery();
                myCommand.Dispose();

                hits++;
            }
            Console.WriteLine("INSERT INTO " + table + ", " + hits + " rows / " + stats.Count + " hosts");
            conn.Close();
        }

        int currentSec = DateTime.Now.Second;
        int currentMin = DateTime.Now.Minute;
        int currentHour = DateTime.Now.Hour;
        int currentDay = DateTime.Now.Day;
        protected override void Use(FrameEthernet frame)
        {
            if (frame.DestinationMAC.Equals(Global.RouterMAC))
            {
                CheckAndAddKey(frame.SourceMAC);
                stats[frame.SourceMAC].UploadedBytes += frame.DataLength;
                stats[frame.SourceMAC].UploadedPackets++;
                stats[frame.SourceMAC].Touch();

                stats[totalMac].UploadedBytes += frame.DataLength;
                stats[totalMac].UploadedPackets++;
                stats[totalMac].Touch();
            }
            if (frame.SourceMAC.Equals(Global.RouterMAC))
            {
                CheckAndAddKey(frame.DestinationMAC);
                stats[frame.DestinationMAC].DownloadedBytes += frame.DataLength;
                stats[frame.DestinationMAC].DownloadedPackets++;
                stats[frame.DestinationMAC].Touch();

                stats[totalMac].DownloadedBytes += frame.DataLength;
                stats[totalMac].DownloadedPackets++;
                stats[totalMac].Touch();
            }
        }
        private void CheckAndAddKey(AddressMAC mac)
        {
            if (!stats.ContainsKey(mac))
            {
                stats.Add(mac, new Stat());
            }
        }

        protected override void HandleException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("TrafficStats: " + ex.Message);
            Console.ResetColor();
        }
    }
}
