﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.DataLink;
using System.Data.SqlClient;
using System.Data;
using sniffer_application.Network.IPv4;
using sniffer_application.Network;

namespace sniffer_application.Database
{
    public static class DBUtils
    {
        private static string connectionString = "";
        public static void SetSettings(string user, string password, string server, string db)
        {
            connectionString = String.Format("user id={0};password={1};server={2};database={3};", user, password, server, db);
        }
        public static SqlConnection CreateConnection()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                return conn;
            }
            catch
            {
                return null;
            }
        }

        private static Dictionary<AddressMAC, int> hostId = new Dictionary<AddressMAC, int>();
        private static readonly object syncLock = new object();
        public static int GetOrCreateHost(AddressMAC mac, SqlConnection conn)
        {
            lock (syncLock)
            {
                if (hostId.ContainsKey(mac)) return hostId[mac];

                SqlDataReader reader = null;
                SqlCommand cmd = new SqlCommand("SELECT Id FROM Host WHERE MacAddress = @MAC", conn);
                cmd.Parameters.Add(new SqlParameter("@MAC", SqlDbType.Binary, 6) { Value = mac.RawData });
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    hostId.Add(mac, reader.GetInt32(0));
                    reader.Close();
                    cmd.Dispose();
                    reader.Dispose();
                    return GetOrCreateHost(mac, conn);
                }

                reader.Close();
                cmd.Dispose();
                reader.Dispose();
                cmd = new SqlCommand("INSERT INTO Host (MacAddress,CreationDate) VALUES(@MAC,@DATE)", conn);
                cmd.Parameters.Add(new SqlParameter("@MAC", SqlDbType.Binary, 6) { Value = mac.RawData });
                cmd.Parameters.Add(new SqlParameter("@DATE", SqlDbType.DateTime, 6) { Value = DateTime.Now });
                cmd.ExecuteNonQuery();
                cmd.Dispose();

                return GetOrCreateHost(mac, conn);
            }
        }

        private static Dictionary<AddressIPv4, int> ipv4Id = new Dictionary<AddressIPv4, int>();
        private static readonly object syncLockIp = new object();
        public static int GetOrCreateIpv4(AddressIPv4 ip, SqlConnection conn)
        {
            lock (syncLockIp)
            {
                if (ipv4Id.ContainsKey(ip)) return ipv4Id[ip];

                SqlDataReader reader = null;
                SqlCommand cmd = new SqlCommand("SELECT Id FROM NetworkAddress WHERE Ipv4 is not null and Ipv4 = @IP", conn);
                cmd.Parameters.Add(new SqlParameter("@IP", SqlDbType.Binary, 6) { Value = ip.RawData() });
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ipv4Id.Add(ip, reader.GetInt32(0));
                    reader.Close();
                    cmd.Dispose();
                    reader.Dispose();
                    return GetOrCreateIpv4(ip, conn);
                }

                reader.Close();
                cmd.Dispose();
                reader.Dispose();
                cmd = new SqlCommand("INSERT INTO NetworkAddress (Ipv4) VALUES(@IP)", conn);
                cmd.Parameters.Add(new SqlParameter("@IP", SqlDbType.Binary, 6) { Value = ip.RawData() });
                cmd.ExecuteNonQuery();
                cmd.Dispose();

                return GetOrCreateIpv4(ip, conn);
            }
        }



        private static Dictionary<AddressIPv4, int> ipv6Id = new Dictionary<AddressIPv4, int>();
        public static int GetOrCreateIp(NetworkAddress ip, SqlConnection conn)
        {
            if (ip is AddressIPv4) return GetOrCreateIpv4(ip as AddressIPv4, conn);

            throw new Exception("Can't find network address");
        }


        
        public static string GetLastSecStat_QUERY = 
@"SELECT MacAddress,DownloadedBytes,UploadedBytes,DownloadedPackets,UploadedPackets 
FROM SecStat 
INNER JOIN (SELECT IdHost, Max(Date) as D FROM SecStat 
GROUP BY IdHost) AS DT 
ON DT.IdHost = SecStat.IdHost and DT.D = SecStat.Date 
INNER JOIN Host 
ON Host.Id = SecStat.IdHost";

        private static string GetTouched =
@"SELECT S.MacAddress FROM
(SELECT MacAddress,DownloadedBytes,UploadedBytes,DownloadedPackets,UploadedPackets 
FROM {0} 
INNER JOIN (SELECT IdHost, Max(Date) as D FROM {0}  
GROUP BY IdHost) AS DT 
ON DT.IdHost = {0}.IdHost and DT.D = {0}.Date 
INNER JOIN Host 
ON Host.Id = {0}.IdHost) AS M RIGHT OUTER JOIN
(SELECT MacAddress,DownloadedBytes,UploadedBytes,DownloadedPackets,UploadedPackets 
FROM SecStat 
INNER JOIN (SELECT IdHost, Max(Date) as D FROM SecStat 
GROUP BY IdHost) AS DT 
ON DT.IdHost = SecStat.IdHost and DT.D = SecStat.Date 
INNER JOIN Host 
ON Host.Id = SecStat.IdHost) AS S ON S.MacAddress = M.MacAddress
WHERE (S.DownloadedBytes != M.DownloadedBytes OR S.UploadedBytes != M.UploadedBytes OR S.DownloadedPackets != M.DownloadedPackets OR S.UploadedPackets != M.UploadedPackets OR M.MacAddress IS NULL)";
        public static AddressMAC[] GetHostsTouched(SqlConnection conn, string table)
        {
            List<AddressMAC> macs = new List<AddressMAC>();
            SqlCommand cmd = new SqlCommand(String.Format(GetTouched, table), conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                macs.Add(new AddressMAC(reader.GetSqlBinary(0).Value, 0));
            }
            reader.Close();
            reader.Dispose();
            cmd.Dispose();

            return macs.ToArray();
        }

        public static void TruncateStats(SqlConnection conn)
        {
            List<AddressMAC> macs = new List<AddressMAC>();
            SqlCommand cmd = new SqlCommand("TRUNCATE TABLE SecStat; TRUNCATE TABLE MinStat; TRUNCATE TABLE HourStat; TRUNCATE TABLE DayStat; ", conn);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }
        public static void TruncateDns(SqlConnection conn)
        {
            List<AddressMAC> macs = new List<AddressMAC>();
            SqlCommand cmd = new SqlCommand("TRUNCATE TABLE DnsQuestionSection; TRUNCATE TABLE DnsResourceRecordCNAME; TRUNCATE TABLE DnsResourceRecordA; TRUNCATE TABLE DnsQuery; ", conn);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        
    }
}
