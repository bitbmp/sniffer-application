﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using sniffer_application.DataLink;
using sniffer_application.Network.IPv4;
using sniffer_application.Transport;
using System.Threading;
using sniffer_application.Database;
using sniffer_application.Analyzer;
using sniffer_application.Stack_TCP_IP.Application.DNS;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace sniffer_application
{
    class Program
    {
        public static IPEndPoint ListenPoint;
        static void Main(string[] args)
        {
            LoadSettings();

            UdpClient cl = new UdpClient(ListenPoint);
            IPEndPoint mit = null;

            TrafficStats trafficStats = new TrafficStats();
            HttpGuardian httpGuardian = new HttpGuardian();
            ContactUpdater contactUpdater = new ContactUpdater();
            BindingUpdater bindingUpdater = new BindingUpdater();
            DnsGuardian dnsGuardian = new DnsGuardian();

            while (true)
            {
                Thread.Sleep(1);
                while (cl.Available > 0)
                {
                    byte[] data = cl.Receive(ref mit);
                    FrameEthernet eth = new FrameEthernet(data);
                    trafficStats.Add(eth);

                    if (eth.EtherType == EtherType.IPv4)
                    {
                        PacketIPv4 packet = new PacketIPv4(eth);
                        bindingUpdater.Add(packet);

                        if (packet.Protocol == ProtocolTypeIPv4.TCP)
                        {
                            SegmentTCP segment = new SegmentTCP(packet);
                            httpGuardian.Add(segment);
                            contactUpdater.Add(segment);
                        }
                        else if (packet.Protocol == ProtocolTypeIPv4.UDP)
                        {
                            DatagramUDP datagram = new DatagramUDP(packet);
                            contactUpdater.Add(datagram);

                            dnsGuardian.Add(datagram);
                        }
                        else
                        {
                         
                        }
                    }
                    
                }

            }
        }

        static void LoadSettings()
        {
            string[] data = File.ReadAllLines("settings.txt");
            Console.WriteLine("Loading settings...");
            try
            {
                ListenPoint = new IPEndPoint(IPAddress.Parse(GetSettingValue("listener_udp_interface",data)), int.Parse(GetSettingValue("listener_udp_port",data)));
            }catch
            {
                Console.WriteLine("Listener port or listener interface not valid");
                Console.ReadKey();
                System.Environment.Exit(1);
            }

            try
            {
                Global.IPv4Subnet = AddressIPv4.Parse(GetSettingValue("subnet", data));
                Global.IPv4SubnetMask = AddressIPv4.Parse(GetSettingValue("subnet_mask", data));
                Global.RouterMAC = AddressMAC.Parse(GetSettingValue("router_mac_address", data));
            }
            catch
            {
                Console.WriteLine("Subnet, subnetmask or router mac not valid");
                Console.ReadKey();
                System.Environment.Exit(1);
            }

            try
            {
                DBUtils.SetSettings(GetSettingValue("db_user_id", data), GetSettingValue("db_password", data), GetSettingValue("db_server_address", data), GetSettingValue("db_name", data));
                var conn = DBUtils.CreateConnection();
                conn.Close();
            }
            catch
            {
                Console.WriteLine("Db settings not valid");
                Console.ReadKey();
                System.Environment.Exit(1);
            }
            
        }
        static string GetSettingValue(string name,string[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                string[] c = data[i].Split('=');
                if (c.Length < 2) continue;

                if (c[0].Trim().ToLower() == name)
                    return c[1].Trim();
            }
            return null;
        }
    }
}
