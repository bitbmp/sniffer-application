﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.Network.IPv4
{
    //https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers
    public enum ProtocolTypeIPv4
    {
        TCP = 0x06,
        UDP = 0x11,
    }
}
