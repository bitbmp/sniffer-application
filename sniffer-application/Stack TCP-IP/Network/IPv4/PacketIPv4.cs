﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.DataLink;

namespace sniffer_application.Network.IPv4
{
    public class PacketIPv4 : NetworkPacket
    {
        public PacketIPv4(DataLinkFrame frame)
        {
            this.UpperLevel = frame;
        }

        public int Version
        {
            get
            {
                return UpperLevel[0] >> 4;
            }
        }
        public int IHL
        {
            get
            {
                return UpperLevel[0] & 0x0F;
            }
        }
        public int DSCP
        {
            get
            {
                return UpperLevel[1] >> 2;
            }
        }
        public int ECN
        {
            get
            {
                return UpperLevel[1] & 0x03;
            }
        }
        public int TotalLength
        {
            get
            {
                return UpperLevel[2] * 0x100 + UpperLevel[3];
            }
        }
        public int Identification
        {
            get
            {
                return UpperLevel[4] * 0x100 + UpperLevel[5];
            }
        }
        public int Flags
        {
            get
            {
                return UpperLevel[6] >> 5;
            }
        }
        public int FragmentOffset
        {
            get
            {
                return (UpperLevel[6] & 0x1F) * 0x100 + UpperLevel[7];
            }
        }
        public int TTL
        {
            get
            {
                return UpperLevel[8];
            }
        }
        public ProtocolTypeIPv4 Protocol
        {
            get
            {
                return (ProtocolTypeIPv4)UpperLevel[9];
            }
        }
        public int HeaderChecksum
        {
            get
            {
                return UpperLevel[10] * 0x100 + UpperLevel[11];
            }
        }
        public override NetworkAddress SourceAddress
        {
            get
            {
                return new AddressIPv4(UpperLevel, 12);
            }
        }
        public override NetworkAddress DestinationAddress
        {
            get
            {
                return new AddressIPv4(UpperLevel, 16);
            }
        }
        public AddressIPv4 SourceAddressIPv4
        {
            get
            {
                return new AddressIPv4(UpperLevel, 12);
            }
        }
        public AddressIPv4 DestinationAddressIPv4
        {
            get
            {
                return new AddressIPv4(UpperLevel, 16);
            }
        }

        public byte[] Options
        {
            get
            {
                return PacketHelper.GetBytes(UpperLevel, 20, (IHL - 5) * 4);
            }
        }
        public override byte[] Payload()
        {
            return PacketHelper.GetBytes(this, 0, DataLength);
        }


        public override int DataLength
        {
            get 
            {
                return this.TotalLength - IHL * 4; 
            }
        }
        public override byte this[int index]
        {
            get 
            {
                return UpperLevel[IHL * 4 + index];
            }
        }
    }
}
