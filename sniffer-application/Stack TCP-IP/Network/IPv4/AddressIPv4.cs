﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.Network.IPv4
{
    public class AddressIPv4 : NetworkAddress
    {
        private byte[] data;

        public AddressIPv4(byte[] data)
        {
            this.data = data;
        }
        public AddressIPv4(Packet packet, int offset)
        {
            this.data = PacketHelper.GetBytes(packet, offset, 4);
        }
        public AddressIPv4(byte b1, byte b2, byte b3, byte b4)
        {
            this.data = new byte[] { b1, b2, b3, b4 };
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(17);
            sb.Append(data[0].ToString("") + ".");
            sb.Append(data[1].ToString("") + ".");
            sb.Append(data[2].ToString("") + ".");
            sb.Append(data[3].ToString(""));
            return sb.ToString();
        }

        public override byte[] RawData()
        {
            return data;
        }

        public bool SameSubnetAs(AddressIPv4 ip, AddressIPv4 subnetmask)
        {
            if ((this.data[0] & subnetmask.data[0]) != ip.data[0]) return false;
            if ((this.data[1] & subnetmask.data[1]) != ip.data[1]) return false;
            if ((this.data[2] & subnetmask.data[2]) != ip.data[2]) return false;
            if ((this.data[3] & subnetmask.data[3]) != ip.data[3]) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return data[0] * 0x10000 + data[1] * 0x100 + data[2] * 0x100 + data[3] * 0x1;
        }
        public override bool Equals(object obj)
        {
            AddressIPv4 ip = obj as AddressIPv4;
            if (ip == null) return false;
            return ip.data[0] == this.data[0] && ip.data[1] == this.data[1] && ip.data[2] == this.data[2] && ip.data[3] == this.data[3];
        }

        public static AddressIPv4 Parse(string value)
        {
            string[] c = value.Split('.');

            byte[] val = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                val[i] = byte.Parse(c[i]);
            }

            return new AddressIPv4(val);
        }
    }
}
