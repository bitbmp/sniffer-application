﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.Network
{
    public abstract class NetworkAddress
    {
        public abstract byte[] RawData();
    }
}
