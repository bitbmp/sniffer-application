﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.DataLink;

namespace sniffer_application.Network
{
    public abstract class NetworkPacket : Packet
    {
        public DataLinkFrame UpperLevel { get; protected set; }
        public abstract NetworkAddress SourceAddress { get; }
        public abstract NetworkAddress DestinationAddress { get; }
    }
}
