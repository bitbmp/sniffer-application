﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.DataLink
{
    public class FrameEthernet : DataLinkFrame
    {
        private byte[] packet;
        public FrameEthernet(byte[] data)
        {
            this.packet = data;
        }

        public override AddressMAC DestinationMAC
        {
            get
            {
                return new AddressMAC(packet, 0);
            }
        }
        public override AddressMAC SourceMAC
        {
            get
            {
                return new AddressMAC(packet, 6);
            }
        }
        public EtherType EtherType
        {
            get
            {
                return (EtherType)(packet[12] * 0x100 + packet[13]);
            }
        }

        public override byte[] Payload()
        {
            return PacketHelper.GetBytes(this, 0, DataLength);
        }
        public override int DataLength
        {
            get
            {
                return packet.Length - 14;
            }
        }
        public override byte this[int index]
        {
            get
            {
                return packet[14 + index];
            }
        }
    }
}
