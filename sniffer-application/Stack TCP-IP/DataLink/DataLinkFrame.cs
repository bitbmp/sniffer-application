﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.DataLink
{
    public abstract class DataLinkFrame : Packet
    {
        public abstract AddressMAC DestinationMAC { get; }
        public abstract AddressMAC SourceMAC { get; }
    }
}
