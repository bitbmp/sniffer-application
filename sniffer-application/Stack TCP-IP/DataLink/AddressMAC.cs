﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace sniffer_application.DataLink
{
    public class AddressMAC
    {
        private byte[] packet;
        private int offset;
        public AddressMAC(byte[] packet, int offset = 0)
        {
            this.packet = packet;
            this.offset = offset;
        }
        public byte[] RawData
        {
            get
            {
                return packet.Skip(offset).Take(6).ToArray();
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(17);
            sb.Append(packet[0 + offset].ToString("X2") + "-");
            sb.Append(packet[1 + offset].ToString("X2") + "-");
            sb.Append(packet[2 + offset].ToString("X2") + "-");
            sb.Append(packet[3 + offset].ToString("X2") + "-");
            sb.Append(packet[4 + offset].ToString("X2") + "-");
            sb.Append(packet[5 + offset].ToString("X2"));
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            AddressMAC mac = obj as AddressMAC;
            if (mac == null) return false;

            return this.packet[offset + 0] == mac.packet[mac.offset + 0] &&
                   this.packet[offset + 1] == mac.packet[mac.offset + 1] &&
                   this.packet[offset + 2] == mac.packet[mac.offset + 2] &&
                   this.packet[offset + 3] == mac.packet[mac.offset + 3] &&
                   this.packet[offset + 4] == mac.packet[mac.offset + 4] &&
                   this.packet[offset + 5] == mac.packet[mac.offset + 5];
        }
        public override int GetHashCode()
        {
            return BitConverter.ToInt32(packet, offset + 1);
        }


        public static AddressMAC Parse(string value)
        {
            string[] c = value.Split(new char[]{':','-'});

            byte[] val = new byte[6];
            for (int i = 0; i < 6; i++)
            {
                val[i] = byte.Parse(c[i], NumberStyles.AllowHexSpecifier);
            }

            return new AddressMAC(val);
        }
    }
}
