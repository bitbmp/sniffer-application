﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.DataLink
{
    //https://en.wikipedia.org/wiki/EtherType
    public enum EtherType
    {
        IPv4 = 0x0800,
        ARP  = 0x0806,
    }
}
