﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Network;

namespace sniffer_application.Transport
{
    public abstract class TransportSegment : Packet
    {
        public NetworkPacket UpperLevel
        {
            get;
            protected set;
        }
        public abstract int SourcePort { get; }
        public abstract int DestinationPort { get; }
    }
}
