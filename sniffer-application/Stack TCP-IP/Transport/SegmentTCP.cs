﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Network;

namespace sniffer_application.Transport
{
    public class SegmentTCP : TransportSegment
    {
        public SegmentTCP(NetworkPacket packet)
        {
            this.UpperLevel = packet;
        }

        public override int SourcePort
        {
            get
            {
                return UpperLevel[0] * 0x100 + UpperLevel[1];
            }
        }
        public override int DestinationPort
        {
            get
            {
                return UpperLevel[2] * 0x100 + UpperLevel[3];
            }
        }
        public uint SequenceNumber
        {
            get
            {
                return UpperLevel[4] * (uint)0x1000000 + UpperLevel[5] * (uint)0x10000 + UpperLevel[6] * (uint)0x100 + UpperLevel[7];
            }
        }
        public uint AcknowledgmentNumber
        {
            get
            {
                return UpperLevel[8] * (uint)0x1000000 + UpperLevel[9] * (uint)0x10000 + UpperLevel[10] * (uint)0x100 + UpperLevel[11];
            }
        }
        public int DataOffset
        {
            get
            {
                return UpperLevel[12] >> 4;
            }
        }
        public class FlagsTCP
        {
            private NetworkPacket packet;
            public FlagsTCP(NetworkPacket packet)
            {
                this.packet = packet;
            }
            public bool NS
            {
                get
                {
                    return (packet[12] & 0x01) > 0;
                }
            }
            public bool CWR
            {
                get
                {
                    return (packet[13] & 0x80) > 0;
                }
            }
            public bool ECE
            {
                get
                {
                    return (packet[13] & 0x40) > 0;
                }
            }
            public bool URG
            {
                get
                {
                    return (packet[13] & 0x20) > 0;
                }
            }
            public bool ACK
            {
                get
                {
                    return (packet[13] & 0x10) > 0;
                }
            }
            public bool PSH
            {
                get
                {
                    return (packet[13] & 0x08) > 0;
                }
            }
            public bool RST
            {
                get
                {
                    return (packet[13] & 0x04) > 0;
                }
            }
            public bool SYN
            {
                get
                {
                    return (packet[13] & 0x02) > 0;
                }
            }
            public bool FIN
            {
                get
                {
                    return (packet[13] & 0x01) > 0;
                }
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder(34);
                if (NS) sb.Append("NS ");
                if (CWR) sb.Append(" CWR ");
                if (ECE) sb.Append(" ECE ");
                if (URG) sb.Append(" URG ");
                if (ACK) sb.Append(" ACK ");
                if (PSH) sb.Append(" PSH ");
                if (RST) sb.Append(" RST ");
                if (SYN) sb.Append(" SYN ");
                if (FIN) sb.Append(" FIN");
                return sb.ToString().Trim();
            }
        }
        public FlagsTCP Flags
        {
            get
            {
                return new FlagsTCP(UpperLevel);
            }
        }
        public int WindowsSize
        {
            get
            {
                return UpperLevel[14] * 0x100 + UpperLevel[15];
            }
        }
        public int Checksum
        {
            get
            {
                return UpperLevel[16] * 0x100 + UpperLevel[17];
            }
        }
        public int UrgentPointer
        {
            get
            {
                return UpperLevel[18] * 0x100 + UpperLevel[19];
            }
        }
        public byte[] Options
        {
            get
            {
                return PacketHelper.GetBytes(UpperLevel, 20, (DataOffset - 5) * 4);
            }
        }

        public override byte[] Payload()
        {
            return PacketHelper.GetBytes(this, 0, DataLength);
        }
        public override int DataLength
        {
            get
            {
                return UpperLevel.DataLength - DataOffset * 4;
            }
        }
        public override byte this[int index]
        {
            get
            {
                return UpperLevel[DataOffset * 4 + index];
            }
        }
    }
}
