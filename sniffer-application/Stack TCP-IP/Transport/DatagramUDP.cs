﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Network;

namespace sniffer_application.Transport
{
    public class DatagramUDP : TransportSegment
    {
       public DatagramUDP(NetworkPacket packet)
        {
            this.UpperLevel = packet;
        }

        public override int SourcePort
        {
            get
            {
                return UpperLevel[0] * 0x100 + UpperLevel[1];
            }
        }
        public override int DestinationPort
        {
            get
            {
                return UpperLevel[2] * 0x100 + UpperLevel[3];
            }
        }
        public int Length
        {
            get
            {
                return UpperLevel[4] * 0x100 + UpperLevel[5];
            }
        }
        public int Checksum
        {
            get
            {
                return UpperLevel[6] * 0x100 + UpperLevel[7];
            }
        }


        public override byte[] Payload()
        {
            return PacketHelper.GetBytes(this, 0, DataLength);
        }
        public override int DataLength
        {
            get
            {
                return UpperLevel.DataLength - 8;
            }
        }
        public override byte this[int index]
        {
            get
            {
                return UpperLevel[8 + index];
            }
        }
    }
}
