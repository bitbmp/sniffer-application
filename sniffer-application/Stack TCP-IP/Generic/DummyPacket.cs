﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.Stack_TCP_IP.Generic
{
    public class DummyPacket : Packet
    {
        private byte[] data;
        public DummyPacket(params byte[] data)
        {
            this.data = data;
        }

        public override byte this[int index]
        {
            get
            {
                return data[index];
            }
        }
        public override int DataLength
        {
            get
            {
                return data.Length;
            }
        }
        public override byte[] Payload()
        {
            return data;
        }
    }
}
