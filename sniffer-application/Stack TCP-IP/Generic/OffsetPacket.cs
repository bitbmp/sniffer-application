﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application
{
    public class OffsetPacket : Packet
    {
        private int offset;
        private Packet packet;
        public OffsetPacket(Packet packet, int offset)
        {
            this.offset = offset;
            this.packet = packet;
        }

        public override byte this[int index]
        {
            get
            {
                return packet[index + offset];
            }
        }
        public override int DataLength
        {
            get
            {
                return packet.DataLength - offset;
            }
        }
        public override byte[] Payload()
        {
            return PacketHelper.GetBytes(packet, offset, DataLength);
        }
        public int Offset
        {
            get
            {
                return offset;
            }
        }
    }
}
