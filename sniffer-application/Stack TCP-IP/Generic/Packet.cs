﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application
{
    public abstract class Packet
    {
        public abstract byte this[int index] { get; }
        public abstract int DataLength { get; }
        public abstract byte[] Payload();
    }
}
