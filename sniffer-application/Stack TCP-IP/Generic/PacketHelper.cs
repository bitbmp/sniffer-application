﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application
{
    public static class PacketHelper
    {
        public static byte[] GetBytes(Packet packet, int offset, int length)
        {
            byte[] data = new byte[length];
            for (int i = 0; i < length; i++)
            {
                data[i] = packet[i + offset];
            }
            return data;
        }
    }
}
