﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Network.IPv4;

namespace sniffer_application.Stack_TCP_IP.Application.DNS
{
    public class ResourceRecord
    {
        private OffsetPacket packet;
        private DnsMessage dnsMessage;
        public ResourceRecord(DnsMessage dnsMessage, int offset)
        {
            this.dnsMessage = dnsMessage;
            this.packet = new OffsetPacket(dnsMessage, offset);
        }

        public int TotalLength
        {
            get
            {
                return NameLength + 10 + ResourceDataLength;
            }
           
        }
        public RRType Type
        {
            get
            {
                return (RRType)(packet[NameLength] * 0x100 + packet[NameLength + 1]);
            }
        }
        public RRClass Class
        {
            get
            {
                return (RRClass)(packet[NameLength + 2] * 0x100 + packet[NameLength + 3]);
            }
        }
        public UInt32 TTL
        {
            get
            {
                return (UInt32)packet[NameLength + 4] * 0x1000000 + (UInt32)packet[NameLength + 5] * 0x10000 + (UInt32)packet[NameLength + 6] * 0x100 + (UInt32)packet[NameLength + 7];
            }
        }
        public UInt16 ResourceDataLength
        {
            get
            {
                return (UInt16)(packet[NameLength + 8] * 0x100 + (UInt16)packet[NameLength + 9]);
            }
        }
        public byte[] ResourceData
        {
            get
            {
                return PacketHelper.GetBytes(packet, NameLength + 10, ResourceDataLength);
            }
        }
        public object ResourceDataObj
        {
            get
            {
                if (Type == RRType.A)
                {
                    return new AddressIPv4(ResourceData);
                }
                else if(Type == RRType.CNAME)
                {
                    return DnsHelper.GetString(dnsMessage, packet.Offset + NameLength + 10);
                }
                else return null;
            }
        }

        public string Name
        {
            get
            {
                return DnsHelper.GetString(dnsMessage, packet.Offset);
            }
        }

        public override string ToString()
        {
            return "Name: " + Name + ", Type: " + Type + ", Class: " + Class + ", TTL: " + TTL + ", RDLength: " + ResourceDataLength;
        }

        private int NameLength
        {
            get
            {
                if ((packet[0] & 0xC0) == 0xC0)
                {
                    return 2;
                }

                int nameLength = 0, i = 0;
                while (packet[i] != 0)
                {
                    nameLength += 1 + packet[i];
                    i += packet[i] + 1;

                    if ((packet[i] & 0xC0) == 0xC0)
                    {
                        return nameLength + 2;
                    }
                }
                return nameLength + 1;
            }
        }
    }
}
