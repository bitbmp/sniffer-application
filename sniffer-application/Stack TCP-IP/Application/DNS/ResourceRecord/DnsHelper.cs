﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.Stack_TCP_IP.Application.DNS
{
    public class DnsHelper
    {
        public static string GetString(DnsMessage message, int startIndex)
        {
            StringBuilder sb = new StringBuilder();

            int i = startIndex;
            if ((message[i] & 0xC0) == 0xC0)
            {
                i = ((message[i] * 0x100 + message[i + 1]) & 0x3FFF) - 12;
            }



            while (message[i] != 0)
            {
                for (int k = 0; k < message[i]; k++)
                {
                    sb.Append((char)(message[i + k + 1]));
                }
                i += message[i] + 1;

                if ((message[i] & 0xC0) == 0xC0)
                {
                    i = ((message[i] * 0x100 + message[i + 1]) & 0x3FFF) - 12;
                }
                sb.Append('.');
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
    }
}
