﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sniffer_application.Stack_TCP_IP.Application.DNS
{
    public class QuestionSection
    {
        private Packet dnsMessage;
        public QuestionSection(DnsMessage dnsMessage, int offset)
        {
            this.dnsMessage = new OffsetPacket(dnsMessage, offset);
        }

        public int TotalLength
        {
            get
            {
                return NameLength + 4;
            }

        }
        public string Name
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                int i = 0;
                while (dnsMessage[i] != 0)
                {
                    for (int k = 0; k < dnsMessage[i]; k++)
                    {
                        sb.Append((char)(dnsMessage[i + k + 1]));
                    }
                    i += dnsMessage[i] + 1;
                    sb.Append('.');
                }
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString();
            }
        }
        public RRType Type
        {
            get
            {
                return (RRType)(dnsMessage[NameLength] * 0x100 + dnsMessage[NameLength + 1]);
            }
        }
        public RRClass Class
        {
            get
            {
                return (RRClass)(dnsMessage[NameLength + 2] * 0x100 + dnsMessage[NameLength + 3]);
            }
        }

        public override string ToString()
        {
            return "QName: "+Name + ", QType: " + Type + ", QClass: " + Class;
        }

        private int NameLength
        {
            get
            {
                int nameLength = 0, i = 0;
                while (dnsMessage[i] != 0)
                {
                    nameLength += 1 + dnsMessage[i];
                    i += dnsMessage[i] + 1;
                }
                return nameLength + 1;
            }
        }
    }
}
