﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sniffer_application.Transport;

namespace sniffer_application.Stack_TCP_IP.Application.DNS
{
    public class DnsMessage : Packet
    {
        private static int headerOffset = 12;
        public static DnsMessage ToDnsMessage(DatagramUDP datagram)
        {
            try
            {
                return new DnsMessage(datagram);
            }
            catch
            {
                return null;
            }
        }
        
        private DatagramUDP datagram;
        public DnsMessage(DatagramUDP datagram)
        {
            this.datagram = datagram;
        }
        public override byte this[int index]
        {
            get
            {
                return datagram[index + headerOffset];
            }
        }
        public override int DataLength
        {
            get
            {
                return datagram.DataLength - headerOffset;
            }
        }
        public override byte[] Payload()
        {
            return PacketHelper.GetBytes(datagram, headerOffset, DataLength);
        }


        public int TransactionID
        {
            get
            {
                return datagram[0] * 0x100 + datagram[1];
            }
        }

        public int NumberOfQuestions
        {
            get
            {
                return datagram[4] * 0x100 + datagram[5];
            }
        }
        public int NumberOfAnswerRRs
        {
            get
            {
                return datagram[6] * 0x100 + datagram[7];
            }
        }
        public int NumberOfAuthorityRRs
        {
            get
            {
                return datagram[8] * 0x100 + datagram[9];
            }
        }
        public int NumberOfAdditionalRRs
        {
            get
            {
                return datagram[10] * 0x100 + datagram[11];
            }
        }


        public QuestionSection[] Questions
        {
            get
            {
                QuestionSection[] quest = new QuestionSection[NumberOfQuestions];
                if (NumberOfQuestions == 0) return quest;

                int sum = 0;
                quest[0] = new QuestionSection(this, sum);
                for (int i = 1; i < quest.Length; i++)
                {
                    quest[i] = new QuestionSection(this, quest[i - 1].TotalLength + sum);
                    sum += quest[i - 1].TotalLength;
                }
                return quest;
            }
        }
        public ResourceRecord[] Answers
        {
            get
            {
                ResourceRecord[] answs = new ResourceRecord[NumberOfAnswerRRs];
                if (NumberOfAnswerRRs == 0) return answs;

                int sum = TotalQuestionsLength();
                answs[0] = new ResourceRecord(this, sum);
                for (int i = 1; i < answs.Length; i++)
                {
                    answs[i] = new ResourceRecord(this, answs[i - 1].TotalLength + sum);
                    sum += answs[i - 1].TotalLength;
                }
                return answs;
            }
        }

        public DatagramUDP UpperLevel
        {
            get
            {
                return datagram;
            }
        }

        private int TotalQuestionsLength()
        {
            int sum = 0;
            for (int i = 0; i < Questions.Length; i++) sum += Questions[i].TotalLength;
            return sum;
        }

        public override string ToString()
        {
            return "[" + TransactionID + " " + NumberOfQuestions + " " + NumberOfAnswerRRs + "]";
        }
    }
}
