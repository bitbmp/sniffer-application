﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Concurrent;

namespace sniffer_application.Utils
{
    public abstract class ThreadedQueue<T>
    {
        private Thread th;
        private SemaphoreSlim sem;
        private ConcurrentQueue<T> data;

        public ThreadedQueue()
        {
            this.sem = new SemaphoreSlim(0);
            this.data = new ConcurrentQueue<T>();

            this.th = new Thread(Run);
            this.th.Start();
        }
        protected abstract void Use(T elem);
        protected virtual void HandleException(Exception ex)
        {
            throw ex;
        }

        public void Add(T elem)
        {
            data.Enqueue(elem);
            sem.Release(1);
        }
        private void Run()
        {
            T elem;
            while (true)
            {
                sem.Wait();
                while (data.Count > 0)
                {
                    if (!data.TryDequeue(out elem))
                    {
                        Console.WriteLine("TryDequeue<" + typeof(T) + "> ThreadedQueue failed");
                        continue;
                    }
                    try
                    {
                        Use(elem);
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);
                    }
                }
            }
        }
        public void Close()
        {
            th.Abort();
            sem.Dispose();
        }
    }
}
